package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.*;

/**
 * A simple CLI (limited functionality).
 */
public class ConsoleUI {
    private static final Logger log = LogManager.getLogger(ConsoleUI.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart cart;

    public ConsoleUI(SalesSystemDAO dao) {
        this.dao = dao;
        cart = new ShoppingCart(dao);
    }

    public static void main(String[] args) throws Exception {
        //SalesSystemDAO dao = new HibernateSalesSystemDAO();
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ConsoleUI console = new ConsoleUI(dao);
        console.run();
    }

    /**
     * Run the sales system CLI.
     */
    public void run() throws Exception {
        System.out.println("===========================");
        System.out.println("=       Sales System      =");
        System.out.println("===========================");
        printUsage();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("> ");
            processCommand(in.readLine().trim().toLowerCase());
            System.out.println("Done. ");
        }
    }

    private void showStock() {
        log.info("Stock tab");
        List<StockItem> stockItems = dao.findStockItems();
        System.out.println("-------------------------");
        for (StockItem si : stockItems) {
            System.out.println(si.getId() + " " + si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items)");
        }
        if (stockItems.size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void showCart() {
        log.info("Cart tab");
        double total = 0;
        System.out.println("-------------------------");
        for (SoldItem si : cart.getAll()) {
            if (si.getQuantity() > 0){
                total = total + si.getPrice()*si.getQuantity();
                System.out.println(si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items)");
            }
        }
        if (cart.getAll().size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
        System.out.println("Total cart price is " + total + " eur.");
        System.out.println("-------------------------");


    }

    private void showTeam() {
        log.info("Team tab");
        System.out.println("-------------------------");

        Properties prop = new Properties();
        File file = new File("../application.properties");

        try {
            InputStream stream = new FileInputStream(file);
            prop.load(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Team name: "+ prop.getProperty("teamname"));
        System.out.println("Team leader: "+ prop.getProperty("leader"));
        System.out.println("Team contact: "+ prop.getProperty("contact"));
        System.out.println("Team member: "+ prop.getProperty("member"));
        System.out.println("-------------------------");

    }

    private void showHistory() throws ParseException {
        Scanner sc = new Scanner(System.in);
        System.out.println("-------------------------");
        System.out.println("a\t\tShow all receipts");
        System.out.println("l\t\tShow last 10 receipts");
        System.out.println("b\t\tShow between 2 dates");
        System.out.println("s\t\tShow receipt by id");
        System.out.println("-------------------------");
        String c = sc.nextLine();
        if (c.equals("a"))
            history("a");
        else if (c.equals("l"))
            history("l");
        else if (c.equals("b"))
            history("b");
        else if (c.equals("s"))
            history("s");
        else {
            System.out.println("unknown command");
        }
    }
    private void history(String command) throws ParseException {

        List<Purchase> purchases = dao.findPurchases();



        if(command.equals("a")){
            historyAll(purchases);
        }
        else if(command.equals("l")){
            historyLast10(purchases);
        }
        else if(command.equals("b")){
            historyBetween(purchases);
        }
        else if(command.equals("s")){
            receiptDetails(purchases);
        }
    }

    private void historyAll(List<Purchase> purchases){
        Collections.sort(purchases, new Comparator<Purchase>() {
            @Override
            public int compare(Purchase p1, Purchase p2) {
                if (p2.getId() > p1.getId())
                    return 1;
                if (p2.getId() < p1.getId())
                    return -1;
                return 0;
            }
        });

        for (Purchase purchase : purchases) {
            System.out.println(purchase.toString());
        }
    }

    private void historyLast10(List<Purchase> purchases) {
        Collections.sort(purchases, new Comparator<Purchase>() {
            @Override
            public int compare(Purchase p1, Purchase p2) {
                if (p2.getId() > p1.getId())
                    return 1;
                if (p2.getId() < p1.getId())
                    return -1;
                return 0;
            }
        });

        if (purchases.size() > 10) { purchases = purchases.subList(0, 10); }
        else { }

        for (Purchase purchase : purchases) {
            System.out.println(purchase.toString());
        }
    }
    private void historyBetween(List<Purchase> purchases) throws ParseException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Insert start Date (YYYY-MM-DD): ");
        String start = sc.nextLine();
        LocalDate startDate = LocalDate.parse(start);
        System.out.println("Insert end Date (YYYY-MM-DD): ");
        String end = sc.nextLine();
        LocalDate endDate = LocalDate.parse(end);
        List<Purchase> purchasesBetweenDates = new ArrayList<>();

        for (Purchase item : purchases) {
            String dateForComparison = item.getDatetime().substring(0,10);
            LocalDate receiptDate = LocalDate.parse(dateForComparison);
            if (startDate.isAfter(receiptDate) || endDate.isBefore(receiptDate) ) { ; } else { purchasesBetweenDates.add(item);}
        }

        Collections.sort(purchasesBetweenDates, new Comparator<Purchase>() {
            @Override
            public int compare(Purchase p1, Purchase p2) {
                if (p2.getId() > p1.getId())
                    return 1;
                if (p2.getId() < p1.getId())
                    return -1;
                return 0;
            }
        });

        for (Purchase purchase : purchases) {
            System.out.println(purchase.toString());
        }

    }
    private void receiptDetails(List<Purchase> purchases) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Insert ID: ");
        Long i = Long.parseLong(sc.nextLine());
        Purchase currentReceipt = dao.findPurchase(i);


        List<SoldItem> receiptItems = new ArrayList<>();
        String[] itemSplitter = currentReceipt.getSoldItemData().split(";");
        for (String element : itemSplitter){
            String [] idSplitter = element.split(":");
            Long itemId = Long.valueOf(idSplitter[0]);
            int itemQuantity = Integer.parseInt(idSplitter[1]);
            StockItem stockItem = dao.findStockItem(itemId);
            SoldItem soldItem = new SoldItem(stockItem, itemQuantity);
            receiptItems.add(soldItem);
        }
        for (SoldItem receiptItem : receiptItems) {
            System.out.println(receiptItem.getId() + " " + receiptItem.getName() + " " + receiptItem.getPrice() + " Euros (" + receiptItem.getQuantity() + " items)");
        }
        System.out.println("Sum: "+currentReceipt.getPurchaseSum());
    }



    private void printUsage() {
        System.out.println("-------------------------");
        System.out.println("Usage:");
        System.out.println("h\t\tShow this help");
        System.out.println("w\t\tShow warehouse contents");
        System.out.println("aw\t\tAdd product to warehouse");
        System.out.println("ew\t\tEdit product in warehouse");
        System.out.println("c\t\tShow cart contents");
        System.out.println("a IDX NR\tAdd NR of stock item with index IDX to the cart");
        System.out.println("cc IDX NR\tChange NR of stock item with index IDX");
        System.out.println("rc IDX\tRemove stock item with index IDX");
        System.out.println("p\t\tPurchase the shopping cart");
        System.out.println("r\t\tReset the shopping cart");
        System.out.println("t\t\tShow team information");
        System.out.println("rh\t\tShow receipts");
        System.out.println("-------------------------");
    }

    private void addWarehouse (Long id, String name, String desc, double price, int quantity){
        dao.beginTransaction();
        dao.saveStockItem(new StockItem(id, name, desc, price, quantity));
        dao.commitTransaction();


    }
    private void editOptions() {
        log.info("Edit/Delete tab");
        System.out.println("-------------------------");
        System.out.println("h\t\tShow options");
        System.out.println("w\t\tShow warehouse contents");
        System.out.println("s\t\tEdit the item by Barcode");
        System.out.println("d\t\tDelete the item by Barcode");
        System.out.println("-------------------------");
    }
    private void processWarehouseCommand (String command) throws Exception {
        String[] c = command.split(" ");
        if (c[0].equals("w"))
            showStock();
        else if (c[0].equals("h"))
            editOptions();
        else if (c[0].equals("s"))
            searchAndEditWarehouse();
        else if (c[0].equals("d"))
            searchAndDeleteWarehouse();
    }
    private void searchAndDeleteWarehouse () throws Exception {
        dao.beginTransaction();
        Scanner input = new Scanner(System.in);
        System.out.println("Enter product Barcode: ");
        Long searchID = input.nextLong();
        List<StockItem> stockItems = dao.findStockItems();
        for(StockItem si: stockItems) {
            if(searchID.equals(si.getId())){
                stockItems.remove(si);
                System.out.println("Item has been removed");
                dao.commitTransaction();
                run();
            }
        }
        input.close();
    }
    private void searchAndEditWarehouse () throws Exception {
        dao.beginTransaction();
        Scanner input = new Scanner(System.in);
        System.out.println("Enter product Barcode: ");
        Long searchID = input.nextLong();
        List<StockItem> stockItems = dao.findStockItems();
        for(StockItem si: stockItems){
            if(searchID.equals(si.getId())){
                System.out.println("Price: ");
                Double price = input.nextDouble();
                System.out.println("Quantity: ");
                int quantity = input.nextInt();
                si.setPrice(price);
                si.setQuantity(quantity);
                System.out.println("Editing complete");
                log.debug("Item price edited to: "+price+" and quantity edited to: "+quantity);
                dao.commitTransaction();
                run();
            }
        }
        System.out.println("Item not found");
        System.out.println("Try again? (Y)");
        String choice = input.next();
            if(choice.equals("Y")) {
                searchAndEditWarehouse();
            }
            else run();
        input.close();
        }


    private void editWarehouse () throws Exception {
        editOptions();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("> ");
            processWarehouseCommand(in.readLine().trim().toLowerCase());
            System.out.println("Done. ");
        }

    }

    private void processCommand(String command) throws Exception {
        String[] c = command.split(" ");

        if (c[0].equals("h"))
            printUsage();
        else if (c[0].equals("q"))
            System.exit(0);
        else if (c[0].equals("w"))
            showStock();
        else if (c[0].equals("c"))
            showCart();
        else if (c[0].equals("p"))
            cart.submitCurrentPurchase();
        else if (c[0].equals("t"))
            showTeam();
        else if (c[0].equals("ew"))
            editWarehouse();
        else if (c[0].equals("r")) {
            System.out.println("Type Y to confirm resetting the shopping cart.");
            Scanner confirm = new Scanner(System.in);
            if (confirm.next().equals("Y")) {
                cart.cancelCurrentPurchase();
            } else {
                System.out.println("Shopping cart not reset.");
            }
            confirm.close();
        }
        else if (c[0].equals("a") && c.length == 3) {
            try {
                long idx = Long.parseLong(c[1]);
                int amount = Integer.parseInt(c[2]);
                if(amount > 0) {
                    StockItem item = dao.findStockItem(idx);
                    if (item != null) {
                        for(StockItem si : dao.findStockItems()){
                            if(si.getId() == idx){
                                if(amount < si.getQuantity()){
                                    cart.addItem(new SoldItem(item, Math.min(amount, item.getQuantity())));
                                }
                                else {
                                    log.error("Amount is higher than warehouse amount");
                                }
                            }
                        }

                    } else {
                        log.error("idx of " + idx + " is not in the Stock");
                        System.out.println("no stock item with id " + idx);
                    }
                }
                else { log.error("Item amount is less than 1"); }
            } catch (SalesSystemException | NumberFormatException e) {
                log.error("Error: "+e.getMessage(), e);
            }
        }
        else if(c[0].equals("cc") && c.length == 3){
            try {
                long idx = Long.parseLong(c[1]);
                int amount = Integer.parseInt(c[2]);
                StockItem item = dao.findStockItem(idx);
                if (item != null) {
                    for (SoldItem si : cart.getAll()) {
                        if(si.getId() == idx) {
                            if (amount < si.getQuantity()){
                                int reset = si.getQuantity();
                                cart.addItem(new SoldItem(item, -reset));
                                cart.addItem(new SoldItem(item, Math.min(amount, item.getQuantity())));
                            }
                            else {
                                log.error("Amount is higher than warehouse amount");
                            }
                        }
                    }
                } else {
                    log.error("idx of "+idx+" is not in the Stock");
                    System.out.println("no stock item with id " + idx);
                }
            } catch (SalesSystemException | NoSuchElementException e) {
                log.error("Error: "+e.getMessage(), e);
            }
        }
        else if(c[0].equals("rc") && c.length == 2){
            try {
                long idx = Long.parseLong(c[1]);
                StockItem item = dao.findStockItem(idx);
                if (item != null) {
                    for (SoldItem si : cart.getAll()) {
                        if(si.getId() == idx){
                            int reset = si.getQuantity();
                            cart.addItem(new SoldItem(item, -reset));
                        }
                    }
                } else {
                    log.error("idx of "+idx+" is not in the Stock");
                    System.out.println("no stock item with id " + idx);
                }
            } catch (SalesSystemException | NoSuchElementException e) {
                log.error("Error: "+e.getMessage(), e);
            }

        }
        else if(c[0].equals("rh")){
            showHistory();
        }
        else if(c[0].equals("aw")) {
            log.info("Add tab");
            Scanner input = new Scanner(System.in);
            System.out.println("Name: ");
            String name = input.nextLine();
            System.out.println("Description: ");
            String desc = input.nextLine();
            System.out.println("Barcode: ");
            Long id = input.nextLong();
            System.out.println("Price: ");
            Double price = input.nextDouble();
            System.out.println("Quantity: ");
            int quantity = input.nextInt();
            log.debug("Item being added: \nid: "+id+"\nname: "+name+"\ndesc: "+desc+"\nprice: "+price+"\nquantity: "+quantity);
            addWarehouse(id, name, desc, price, quantity);
            System.out.println(name + " successfully added to warehouse!");
            //input.close();
        }
        else {
            log.error("Wrong command");
            System.out.println("unknown command");
        }
    }


}
