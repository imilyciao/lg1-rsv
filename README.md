# Team LG1-RSV:
1. Ragnar Kadai
2. Vladimir Lijepa

## Homework 1:
[Intermediate results](https://bitbucket.org/imilyciao/lg1-rsv/wiki/Homework%201%20intermediate%20results)

[Interview questions](https://bitbucket.org/imilyciao/lg1-rsv/wiki/Homework%201%20interview%20questions)

[Homework](https://bitbucket.org/imilyciao/lg1-rsv/wiki/Homework%201)

## Homework 2:
[Use cases](https://bitbucket.org/imilyciao/lg1-rsv/wiki/Homework%202%20Use%20cases)

[Class diagram](https://bitbucket.org/imilyciao/lg1-rsv/wiki/Homework%202%20Class%20diagram)

## Homework 3:
[Homework 3 Tag](https://bitbucket.org/imilyciao/lg1-rsv/commits/tag/homework-3)

## Homework 4:
[Task 1 answers](https://bitbucket.org/imilyciao/lg1-rsv/wiki/Homework%204%20Task%201)

[Homework 4 Tag](https://bitbucket.org/imilyciao/lg1-rsv/commits/tag/homework-4)

## Homework 5:
[Homework 5 Tag](https://bitbucket.org/imilyciao/lg1-rsv/commits/tag/homework-5)

## Homework 6:
[Test plan](https://bitbucket.org/imilyciao/lg1-rsv/wiki/Homework%206%20Task%203)

## Homework 7:
[Homework 7](https://bitbucket.org/imilyciao/lg1-rsv/wiki/Homework%207)