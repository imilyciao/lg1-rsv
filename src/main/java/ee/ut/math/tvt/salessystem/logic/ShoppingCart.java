package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ShoppingCart {

    private static final Logger log = LogManager.getLogger(ShoppingCart.class);

    private final SalesSystemDAO dao;
    private final List<SoldItem> items = new ArrayList<>();

    public ShoppingCart(SalesSystemDAO dao) {
        this.dao = dao;
    }

    /**
     * Add new SoldItem to table.
     */
    public void addItem(SoldItem item) throws Exception {
        // TODO In case such stockItem already exists increase the quantity of the existing stock
        // TODO verify that warehouse items' quantity remains at least zero or throw an exception
        if (item.getQuantity() <= item.getStockItem().getQuantity() && item.getQuantity() >= 0) {
            // if state remains 0, product is added as separate entry,
            // if it becomes 1, product quantity is added, if it becomes 2, max quantity is exceeded
            int state = 0;
            Long itemId = item.getId();
            for (SoldItem cartEntry : items) {
                Long cartEntryId = cartEntry.getId();
                if (cartEntryId == itemId) {
                    if (item.getStockItem().getQuantity() >= item.getQuantity() + cartEntry.getQuantity()) {
                        cartEntry.setQuantity(cartEntry.getQuantity() + item.getQuantity());
                        state = 1;
                    } else state = 2;
                }
            }
            if (state == 0) {
                items.add(item);
                log.debug("Added " + item.getName() + " quantity of " + item.getQuantity());
            }
            else if (state == 1){

            }
            else if (state == 2){
                throw new Exception("Item quantity too large");
            }
        }
        else{
            throw new Exception("Item quantity is negative");
        }
    }

    public List<SoldItem> getAll() {
        return items;
    }

    public void cancelCurrentPurchase() {
        items.clear();
    }

    public void submitCurrentPurchase() {
        // TODO decrease quantities of the warehouse stock

        // note the use of transactions. InMemorySalesSystemDAO ignores transactions
        // but when you start using hibernate in lab5, then it will become relevant.
        // what is a transaction? https://stackoverflow.com/q/974596
        log.info("Transaction is being submitted");

        List<SoldItem> purchaseItems = new ArrayList<>();
        String soldItemData = "";

        dao.beginTransaction();
        try {
        	String fileName = "receipts/" + new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss'.txt'").format(new Date());
            FileWriter receipt = new FileWriter(fileName);
            double sum = 0.0;
            for (SoldItem item : items) {
                soldItemData += ";" + item.getId() + ":" + item.getQuantity();
                purchaseItems.add(item);
            	receipt.write(item.getId() + ";" + item.getName() + ";" + item.getPrice() + ";" + item.getQuantity() + "\n");
                StockItem ItemInStock = dao.findStockItem(item.getId());
                sum += item.getPrice()*item.getQuantity();
                ItemInStock.setQuantity(ItemInStock.getQuantity() - item.getQuantity());
                dao.saveSoldItem(item);
            }
            String datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
            String date = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
            String time = new SimpleDateFormat("HH:mm:ss").format(new Date());
            String data;
            try{
                data = soldItemData.substring(1, soldItemData.length());
            }
            catch(Exception e){
                data = "";
            }
            Purchase purchase = new Purchase(new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date()), data, date, time, sum);
            dao.savePurchase(purchase);
            dao.commitTransaction();
            receipt.close();
            items.clear();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            log.error("Transaction is being rolled back");
            dao.rollbackTransaction();
            throw e;
        }
    }
}
