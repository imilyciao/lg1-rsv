package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class HibernateSalesSystemDAO implements SalesSystemDAO {

    private final EntityManagerFactory emf;
    private final EntityManager em;

    public HibernateSalesSystemDAO() {
        // if you get ConnectException/JDBCConnectionException then you
        // probably forgot to start the database before starting the application
        emf = Persistence.createEntityManagerFactory("pos");
        em = emf.createEntityManager();
    }

    // TODO implement missing methods

    public void close() {
        em.close();
        emf.close();
    }

    @Override
    public List<StockItem> findStockItems() {
        return em.createQuery("from StockItem", StockItem.class).getResultList();
    }

    @Override
    public List<SoldItem> findSoldItems() {
        return em.createQuery("from SoldItem", SoldItem.class).getResultList();
    }

    @Override
    public StockItem findStockItem(long id) {
        List<StockItem> stockItemList = findStockItems();
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        if(stockItem.getQuantity() >= 0){
            em.merge(stockItem);
        }
    }

    @Override
    public SoldItem findSoldItem(long id) {
        List<SoldItem> soldItemList = findSoldItems();
        for (SoldItem item : soldItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        em.merge(item);
    }

    @Override
    public void deleteStockItem(StockItem item) { em.remove(item); }

    @Override
    public void savePurchase(Purchase item) { em.merge(item); }

    @Override
    public List<Purchase> findPurchases() {
        return em.createQuery("from Purchase", Purchase.class).getResultList();
    }

    @Override
    public Purchase findPurchase(long id) {
        List<Purchase> purchaseList = findPurchases();
        for (Purchase item : purchaseList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }


    @Override
    public void beginTransaction() {
        em.getTransaction().begin();
    }

    @Override
    public void rollbackTransaction() {
        em.getTransaction().rollback();
    }

    @Override
    public void commitTransaction() {
        em.getTransaction().commit();
    }
}
