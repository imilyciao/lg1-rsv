package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;

@Entity
@Table(name = "PURCHASE")
public class Purchase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "datetime")
    private String datetime;

    @Column(name = "date")
    private String date;

    @Column(name = "purchasesum")
    private double purchaseSum;

    @Column(name = "time")
    private String time;

    @Column(name = "solditemdata")
    private String soldItemData;

    public Purchase() {
    }

    public Purchase(String datetime, String soldItemData, String date, String time, double purchaseSum) {
        this.datetime = datetime;
        this.soldItemData = soldItemData;
        this.date = date;
        this.time = time;
        this.purchaseSum = purchaseSum;
    }

    public String getDatetime() {
        return datetime;
    }
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getSoldItemData() {
        return soldItemData;
    }

    public String date() {
        return datetime.substring(0, 10);
    }

    public String time() {
        return datetime.substring(datetime.length() - 8);
    }

    public void setSoldItemData(String soldItemData) {
        this.soldItemData = soldItemData;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Receipt{" +
                "id='" + id + '\'' +
                "date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", sum='" + purchaseSum + '\'' +
                '}';
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getPurchaseSum() {
        return purchaseSum;
    }

    public void setPurchaseSum(double purchaseSum) {
        this.purchaseSum = purchaseSum;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
