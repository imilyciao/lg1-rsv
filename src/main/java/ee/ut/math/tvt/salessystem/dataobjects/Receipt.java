package ee.ut.math.tvt.salessystem.dataobjects;

public class Receipt {
	
	private String date;
	private String time;
	private String sum;
	private String filename;
	
	public Receipt() {
    }

    public Receipt(String date, String time, String sum, String filename) {
        this.date = date;
        this.time = time;
        this.sum = sum;
        this.filename = filename;
    }
	
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getSum() {
		return sum;
	}
	public void setSum(String sum) {
		this.sum = sum;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	@Override
	public String toString() {
		return "Receipt{" +
				"date='" + date + '\'' +
				", time='" + time + '\'' +
				", sum='" + sum + '\'' +
				", filename='" + filename + '\'' +
				'}';
	}
}
