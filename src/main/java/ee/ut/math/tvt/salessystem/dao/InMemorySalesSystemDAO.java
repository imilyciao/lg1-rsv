package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.Receipt;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.util.ArrayList;
import java.util.List;

public class InMemorySalesSystemDAO implements SalesSystemDAO {

    private final List<StockItem> stockItemList;
    private final List<SoldItem> soldItemList;
    private final List<Receipt> receiptList;

    public InMemorySalesSystemDAO() {
        List<StockItem> items = new ArrayList<StockItem>();
        items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
        items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
        items.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
        items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
        this.stockItemList = items;
        this.soldItemList = new ArrayList<>();
        this.receiptList = new ArrayList<>();
    }

    @Override
    public List<StockItem> findStockItems() {
        return stockItemList;
    }

    @Override
    public List<SoldItem> findSoldItems() {
        return soldItemList;
    }
    
    public List<Receipt> findReceiptList() {
    	return receiptList;
    }

    @Override
    public StockItem findStockItem(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public SoldItem findSoldItem(long id) {
        for (SoldItem item : soldItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }


    @Override
    public void saveSoldItem(SoldItem item) {
        soldItemList.add(item);
    }

    @Override
    public void deleteStockItem(StockItem item) {

    }

    @Override
    public void savePurchase(Purchase item) {

    }

    @Override
    public List<Purchase> findPurchases() {
        return null;
    }

    @Override
    public Purchase findPurchase(long id) {
        return null;
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        if(stockItem.getQuantity() >= 0){
            for (StockItem item : stockItemList) {
                if(stockItem.getId() == item.getId()){
                    item.setQuantity(item.getQuantity() + stockItem.getQuantity());
                    break;
                }
            }
            stockItemList.add(stockItem);
        }
    }

    @Override
    public void beginTransaction() {
    }

    @Override
    public void rollbackTransaction() {
    }

    @Override
    public void commitTransaction() {
        for (StockItem stockItem : stockItemList) {
            
        }
    }
}
