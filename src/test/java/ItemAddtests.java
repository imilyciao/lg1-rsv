import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import static junit.framework.TestCase.assertTrue;

public class ItemAddtests {

    private static final Logger log = LogManager.getLogger(ItemAddtests.class);
    InMemorySalesSystemDAO dao = new InMemorySalesSystemDAO();
    ShoppingCart cart = new ShoppingCart(dao);

    @Test
    @DisplayName("check that methods beginTransaction and commitTransaction are both called exactly once and that order")
    public void testAddingItemBeginsAndCommitsTransaction() {
        int begin = 0, commit = 0;
        dao.beginTransaction();
        begin++;
        dao.commitTransaction();
        commit++;
        assertTrue(begin >= commit);
    }

    @Test
    @DisplayName("check that a new item is saved through the DAO")
    public void testAddingNewItem() {
        StockItem stockItem = new StockItem(10L, "Red Bull", "Energy drink", 1.50, 20);
        dao.saveStockItem(stockItem);
        if (dao.findStockItem(stockItem.getId()) == null) {
            log.info(dao.findStockItems());
            Assert.fail("Did not find saved item");
        }
        else log.info("Saved item: " + stockItem + " was found in the DAO");
    }

    @Test
    @DisplayName("check that adding a new item increases the quantity and the saveStockItem method of the DAO is not called")
    public void testAddingExistingItem() {
        StockItem stockItem = new StockItem(1L, "Lays chips", "Potato chips", 11.0, 6);
        long id = stockItem.getId();
        int before = dao.findStockItem(id).getQuantity();
        log.info("The quantity of existing item: "+before);
        log.info("The quantity of new item: "+stockItem.getQuantity());
        for (StockItem item : dao.findStockItems()) {
            if(item.getId() == stockItem.getId()){
                item.setQuantity(item.getQuantity() + stockItem.getQuantity());
            }
        }
        log.info("Item quantity now: "+dao.findStockItem(id).getQuantity());
        if (dao.findStockItem(id).getQuantity() > before) {
            log.info("Item quantity has increased. Success");
        } else {
            Assert.fail("Item quantity did not increase");
        }
    }

    @Test
    @DisplayName("check that adding an item with negative quantity results in an exception")
    public void testAddingItemWithNegativeQuantity() {
        StockItem stockItem = new StockItem(10L, "Red Bull", "Energy drink", 1.50, -20);
        dao.saveStockItem(stockItem);
        if (dao.findStockItem(stockItem.getId()) != null) {
            Assert.fail("Item with negative quantity was added");
        } else {
            log.info("Item with negative quantity was not added");
        }
    }
}
