import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InOrder;

import java.util.Date;

import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;


public class ShoppingCartTests {

    private static final Logger log = LogManager.getLogger(ShoppingCartTests.class);
    InMemorySalesSystemDAO dao = new InMemorySalesSystemDAO();
    ShoppingCart cart = new ShoppingCart(dao);

    @Test
    public void testAddingExistingItem() throws Exception {
        SoldItem soldItem1 = new SoldItem(dao.findStockItem(1L), 1);
        SoldItem soldItem2 = new SoldItem(dao.findStockItem(1L), 3);
        int before = soldItem1.getQuantity();
        cart.addItem(soldItem1);
        cart.addItem(soldItem2);
        log.info("Shopping cart item quantity before "+before);
        if(cart.getAll().get(0).getQuantity() > before){
            log.info("Adding existing item increases quantity in shopping cart "+cart.getAll().get(0).getQuantity());
        }
        else{
            Assert.fail("Item quantity did not change");
        }

    }

    @Test
    public void testAddingNewItem() throws Exception {
        SoldItem soldItem = new SoldItem(dao.findStockItem(1L), 3);
        cart.addItem(soldItem);
        if(cart.getAll().size() > 0){
            log.info("Item added to cart");
        }
        else{
            Assert.fail("Item was not added to cart");
        }

    }

    @Test
    public void testAddingItemWithNegativeQuantity() {
        SoldItem soldItem = new SoldItem(dao.findStockItem(1L), -5);
        try{
            cart.addItem(soldItem);
            Assert.fail("Item with negative quantity was added");
        }
        catch(Exception e){
            log.info("Exception was caught adding negative item to cart");
        }
    }

    @Test
    public void testAddingItemWithQuantityTooLarge() {
        SoldItem soldItem = new SoldItem(dao.findStockItem(1L), 10);
        try{
            cart.addItem(soldItem);
            Assert.fail("Item with quantity exceeding stock was added");
        }
        catch(Exception e){
            log.info("Exception was caught adding item with quantity over stock quantity");
        }
    }

    @Test
    public void testAddingItemWithQuantitySumTooLarge() throws Exception {
        SoldItem soldItem1 = new SoldItem(dao.findStockItem(1L), 4);
        cart.addItem(soldItem1);
        SoldItem soldItem2 = new SoldItem(dao.findStockItem(1L), 50);
        try{
            cart.addItem(soldItem2);
            Assert.fail("Item with quantity exceeding stock was added");
        }
        catch(Exception e){
            log.info("Exception was caught adding item with quantity over stock quantity");
        }
    }

    @Test
    public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity() throws Exception {
        //TODO
        StockItem item = dao.findStockItem(2L);
        int before = dao.findStockItem(2L).getQuantity();
        cart.addItem(new SoldItem(item, 2));
        cart.submitCurrentPurchase();
        log.info("Old quantity: "+before+" New quantity: "+item.getQuantity());
        if(item.getQuantity() == before){
            Assert.fail("Quantity did no decrease");
        }
        else{

            log.info("Quantity did decrease");
        }
    }

    @Test
    public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction() throws Exception {
        InMemorySalesSystemDAO daoMock = mock(InMemorySalesSystemDAO.class);
        ShoppingCart cartMock = new ShoppingCart(daoMock);
        cartMock.submitCurrentPurchase();
        verify(daoMock, times(1)).beginTransaction();
        verify(daoMock, times(1)).commitTransaction();
        InOrder inOrder = inOrder(daoMock);
        inOrder.verify(daoMock).beginTransaction();
        inOrder.verify(daoMock).commitTransaction();
    }

    @Test
    public void testSubmittingCurrentOrderCreatesHistoryItem() throws Exception {
        StockItem item = dao.findStockItem(2L);
        cart.addItem(new SoldItem(item, 1));
        cart.submitCurrentPurchase();
        if(dao.findSoldItem(2L) != null){
            log.info("Sold item was saved");
        }
        else{
            Assert.fail("Item was not saved");
        }
    }

    @Test
    public void testSubmittingCurrentOrderSavesCorrectTime() throws Exception {
        Date start = new Date();
        log.info(start);
        StockItem item = dao.findStockItem(2L);
        cart.addItem(new SoldItem(item, 2));
        cart.submitCurrentPurchase();
        Date end = new Date();
        log.info(end);
        if(start.getTime()+200 >= end.getTime() && dao.findSoldItem(2L) != null){
            log.info("Item was saved and contains the correct item");
        }
        else if(start.getTime()+200 >= end.getTime() && dao.findSoldItem(2L) == null){
            Assert.fail("Correct item was not saved");
        }
        else{
            Assert.fail("Time difference");
        }
    }

    @Test
    public void testCancellingOrder() throws Exception {
        StockItem item1 = dao.findStockItem(1L);
        StockItem item2 = dao.findStockItem(2L);
        cart.addItem(new SoldItem(item1, 2));
        cart.cancelCurrentPurchase();
        cart.addItem(new SoldItem(item2, 2));
        cart.submitCurrentPurchase();
        if(dao.findSoldItem(1L) == null){
            log.info("Item 1 has not been saved");
        }
        else{
            Assert.fail("Item 1 was saved to purchase");
        }
    }

    @Test
    public void testCancellingOrderQuanititesUnchanged() throws Exception {
        StockItem item = dao.findStockItem(1L);
        int before = item.getQuantity();
        cart.addItem(new SoldItem(item, 2));
        log.info(item.getQuantity());
        cart.cancelCurrentPurchase();
        if(item.getQuantity() == before){
            log.info("Cancelling purchase made quantity default");
        }
        else{
            Assert.fail("Cancelling purchase did not change quantity");
        }
    }
}

