package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "Point-of-sale" in the menu). Consists of the purchase menu,
 * current purchase dialog and shopping cart table.
 */
public class PurchaseController implements Initializable {

    private static final Logger log = LogManager.getLogger(PurchaseController.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart shoppingCart;

    @FXML
    private Button newPurchase;
    @FXML
    private Button submitPurchase;
    @FXML
    private Button cancelPurchase;
    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private Spinner<String> nameField;
    @FXML
    private TextField priceField;
    @FXML
    private Button addItemButton;
    @FXML
    private TableView<SoldItem> purchaseTableView;
    @FXML
    private Button removeButton;
    @FXML
    private Text totalPrice;
    @FXML
    private TableColumn <SoldItem, Integer> quantityColumn;


    public PurchaseController(SalesSystemDAO dao, ShoppingCart shoppingCart) {
        this.dao = dao;
        this.shoppingCart = shoppingCart;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        log.info("Purchase tab has been opened");
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        purchaseTableView.setItems(FXCollections.observableList(shoppingCart.getAll()));
        disableProductField(true);
        setupQuantityColumn();
        setTableEditable();

        this.barCodeField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    fillInputsBySelectedStockItem();
                }
            }
        });
    }

    @FXML
    public void removeButtonClicked() {
        purchaseTableView.getItems().removeAll(purchaseTableView.getSelectionModel().getSelectedItem());

        purchaseTableView.setItems(FXCollections.observableList(shoppingCart.getAll()));
        purchaseTableView.refresh();
        double total = 0 ;
        for (SoldItem item : purchaseTableView.getItems()) {
            total = total + item.getPrice();
        }
        totalPrice.setText("Total price: " + total);
    }

    /** Event handler for the <code>new purchase</code> event. */
    @FXML
    protected void newPurchaseButtonClicked() {
        log.debug("New purchase button has been clicked");
        try {
            enableInputs();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>cancel purchase</code> event.
     */
    @FXML
    protected void cancelPurchaseButtonClicked() {
        log.debug("Purchase cancel button has been clicked");
        String[] options = {"yes", "no"};
        int x = JOptionPane.showOptionDialog(null,
                "Are you sure you want to cancel purchase?","Confirm",
                JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE,null,options,options[0]);
        if(x == 0) {
            try {
                shoppingCart.cancelCurrentPurchase();
                disableInputs();
                purchaseTableView.refresh();
            } catch (SalesSystemException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    /**
     * Event handler for the <code>submit purchase</code> event.
     */
    @FXML
    protected void submitPurchaseButtonClicked() {
        log.debug("Purchase submit button has been clicked");
        try {
            log.debug("Contents of the current basket:\n" + shoppingCart.getAll());
            shoppingCart.submitCurrentPurchase();
            disableInputs();
            purchaseTableView.refresh();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    // switch UI to the state that allows to proceed with the purchase
    private void enableInputs() {
        resetProductField();
        disableProductField(false);
        cancelPurchase.setDisable(false);
        submitPurchase.setDisable(false);
        newPurchase.setDisable(true);
    }

    // switch UI to the state that allows to initiate new purchase
    private void disableInputs() {
        resetProductField();
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        newPurchase.setDisable(false);
        disableProductField(true);
    }

    private void fillInputsBySelectedStockItem() {
        StockItem stockItem = getStockItemByBarcode();
        if (stockItem != null) {
            nameField.setPromptText(stockItem.getName());
            priceField.setText(String.valueOf(stockItem.getPrice()));
        } else {
            resetProductField();
        }
    }

    // Search the warehouse for a StockItem with the bar code entered
    // to the barCode textfield.
    private StockItem getStockItemByBarcode() {
        try {
            long code = Long.parseLong(barCodeField.getText());
            return dao.findStockItem(code);
        } catch (NumberFormatException e) {
            log.error("NumberFormatException caught");
            return null;
        }
    }

    /**
     * Add new item to the cart.
     */
    @FXML
    public void addItemEventHandler() throws Exception {
        log.debug("Chosen item is being added to cart");
        // add chosen item to the shopping cart.
        StockItem stockItem = getStockItemByBarcode();
        if (stockItem != null) {
            int quantity;
            try {
                quantity = Integer.parseInt(quantityField.getText());
                if(quantity > 0) {
                    if(quantity <= stockItem.getQuantity()) {
                        shoppingCart.addItem(new SoldItem(stockItem, quantity));
                        log.debug("Item: "+stockItem+" with a quantity of: "+quantity+" has been added to the cart");
                        purchaseTableView.refresh();
                    }
                    else {
                        JOptionPane.showMessageDialog(null, "Amount is higher than warehouse quantity");
                    }
                }
                else {
                    log.error("Amount less than 1");
                    JOptionPane.showMessageDialog(null, "Amount is less than 1");
                }
            } catch (NumberFormatException e) {
                log.error("Amount is not integer");
                JOptionPane.showMessageDialog(null, "Amount is not integer");
            }
        }
        double total = 0 ;
        for (SoldItem item : purchaseTableView.getItems()) {
            total = total + item.getPrice()*item.getQuantity();
        }
        totalPrice.setText("Total price: " + total);



    }

    /**
     * Sets whether or not the product component is enabled.
     */
    private void disableProductField(boolean disable) {
        this.addItemButton.setDisable(disable);
        this.barCodeField.setDisable(disable);
        this.quantityField.setDisable(disable);
        this.nameField.setDisable(disable);
        this.priceField.setDisable(disable);
    }

    /**
     * Reset dialog fields.
     */
    private void resetProductField() {
        log.debug("Resetting fields\nbarcode: "+barCodeField+"\nquantity: "+quantityField+"\nname: "+nameField+"\nprice: "+priceField);
        barCodeField.setText("");
        quantityField.setText("1");
        nameField.setPromptText("");
        priceField.setText("");
        nameField.setEditable(false);
        priceField.setEditable(false);
    }

    private TableColumn< SoldItem, ? > getTableColumn(
            final TableColumn < SoldItem, ? > column, int offset) {
        int columnIndex = purchaseTableView.getVisibleLeafIndex(column);
        int newColumnIndex = columnIndex + offset;
        return purchaseTableView.getVisibleLeafColumn(newColumnIndex);
    }

    @SuppressWarnings("unchecked")
    private void editFocusedCell() {
        final TablePosition<SoldItem, ? > focusedCell = purchaseTableView
                .focusModelProperty().get().focusedCellProperty().get();
        purchaseTableView.edit(focusedCell.getRow(), focusedCell.getTableColumn());
    }

    @SuppressWarnings("unchecked")
    private void selectPrevious() {
        if (purchaseTableView.getSelectionModel().isCellSelectionEnabled()) {
            // in cell selection mode, we have to wrap around, going from
            // right-to-left, and then wrapping to the end of the previous line
            TablePosition <SoldItem, ? > pos = purchaseTableView.getFocusModel()
                    .getFocusedCell();
            if (pos.getColumn() - 1 >= 0) {
                // go to previous row
                purchaseTableView.getSelectionModel().select(pos.getRow(),
                        getTableColumn(pos.getTableColumn(), -1));
            } else if (pos.getRow() < purchaseTableView.getItems().size()) {
                // wrap to end of previous row
                purchaseTableView.getSelectionModel().select(pos.getRow() - 1,
                        purchaseTableView.getVisibleLeafColumn(
                                purchaseTableView.getVisibleLeafColumns().size() - 1));
            }
        } else {
            int focusIndex = purchaseTableView.getFocusModel().getFocusedIndex();
            if (focusIndex == -1) {
                purchaseTableView.getSelectionModel().select(purchaseTableView.getItems().size() - 1);
            } else if (focusIndex > 0) {
                purchaseTableView.getSelectionModel().select(focusIndex - 1);
            }
        }
    }


    private void setTableEditable() {
        log.debug("Table is editable");
        purchaseTableView.setEditable(true);
        // allows the individual cells to be selected
        purchaseTableView.getSelectionModel().cellSelectionEnabledProperty().set(true);
        // when character or numbers pressed it will start edit in editable
        // fields
        purchaseTableView.setOnKeyPressed(event -> {
            if (event.getCode().isLetterKey() || event.getCode().isDigitKey()) {
                editFocusedCell();
            } else if (event.getCode() == KeyCode.RIGHT ||
                    event.getCode() == KeyCode.TAB) {
                purchaseTableView.getSelectionModel().selectNext();
                event.consume();
            } else if (event.getCode() == KeyCode.LEFT) {
                // work around due to
                // TableView.getSelectionModel().selectPrevious() due to a bug
                // stopping it from working on
                // the first column in the last row of the table
                selectPrevious();
                event.consume();
            }
        });
    }

    private void setupQuantityColumn() {

        quantityColumn.setCellFactory(EditCell. <SoldItem, Integer > forTableColumn(
                new MyIntegerStringConverter()));

        quantityColumn.setOnEditCommit(event -> {
            final Integer value = event.getNewValue() != null ? event.getNewValue() :
                    event.getOldValue();
            ((SoldItem) event.getTableView().getItems()
                    .get(event.getTablePosition().getRow()))
                    .setQuantity(value);
            purchaseTableView.refresh();
        });
    }
}
