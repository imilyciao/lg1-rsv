package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.net.URL;
import java.time.LocalDate;
import java.util.*;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */
public class HistoryController implements Initializable {

    private static final Logger log = LogManager.getLogger(HistoryController.class);
    
    private final SalesSystemDAO dao;
    
    @FXML
    private Button showAllButton;
    
    @FXML
    private Button showBetweenDatesButton;
    
    @FXML
    private Button showLast10Button;
    
    @FXML
    private DatePicker startDate;
    
    @FXML
    private DatePicker endDate;

	@FXML
	private TableView<Purchase> purchaseTable;
    
    @FXML
    private TableView<SoldItem> cartTable;
    
    public HistoryController(SalesSystemDAO dao) {
        this.dao = dao;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        log.info("History tab has been opened");
        purchaseTable.setRowFactory(tv -> {
            TableRow<Purchase> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    Purchase rowData = row.getItem();
                    System.out.println("Double click on: "+rowData.getSoldItemData());
                    displayPurchaseData(rowData.getSoldItemData());
                }
            });
            return row ;
        });

    }



	public void displayPurchaseData(String soldItemsData) {
        log.debug("Receipt details are shown");
		List<SoldItem> receiptItems = new ArrayList<>();
        String[] itemSplitter = soldItemsData.split(";");
        for (String element : itemSplitter){
            String [] idSplitter = element.split(":");
            Long itemId = Long.valueOf(idSplitter[0]);
            int itemQuantity = Integer.parseInt(idSplitter[1]);
            StockItem stockItem = dao.findStockItem(itemId);
            SoldItem soldItem = new SoldItem(stockItem, itemQuantity);
            receiptItems.add(soldItem);
        }

		cartTable.setItems(FXCollections.observableList(receiptItems));


	}

	public void showLast10ButtonPressed() {
        log.debug("Last 10 button has been clicked");
        List<Purchase> purchases = dao.findPurchases();

        Collections.sort(purchases, new Comparator<Purchase>() {
            @Override
            public int compare(Purchase p1, Purchase p2) {
                if (p2.getId() > p1.getId())
                    return 1;
                if (p2.getId() < p1.getId())
                    return -1;
                return 0;
            }
        });

        if (purchases.size() > 10) { purchases = purchases.subList(0, 10); }
        else { }

        purchaseTable.setItems(FXCollections.observableList(purchases));
    }

	public void showBetweenDatesButtonPressed() {
        log.debug("Between dates button has been clicked");
        LocalDate startDateValue = startDate.getValue();

        LocalDate endDateValue = endDate.getValue();

        List<Purchase> purchases = dao.findPurchases();

        List<Purchase> purchasesBetweenDates = new ArrayList<>();

        try {
            for (Purchase item : purchases) {
                String dateForComparison = item.getDatetime().substring(0, 10);
                LocalDate receiptDate = LocalDate.parse(dateForComparison);
                if (startDateValue.isAfter(receiptDate) || endDateValue.isBefore(receiptDate)) {
                    ;

                } else {
                    purchasesBetweenDates.add(item);

                }
            }
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Date inputs are incorrect!");
        }

        Collections.sort(purchasesBetweenDates, new Comparator<Purchase>() {
            @Override
            public int compare(Purchase p1, Purchase p2) {
                if (p2.getId() > p1.getId())
                    return 1;
                if (p2.getId() < p1.getId())
                    return -1;
                return 0;
            }
        });

        purchaseTable.setItems(FXCollections.observableList(purchasesBetweenDates));

    }

	public void showAllButtonPressed() {
        log.debug("All history button has been clicked");
		List<Purchase> purchases = dao.findPurchases();
        log.info(purchases);
        Collections.sort(purchases, new Comparator<Purchase>() {
            @Override
            public int compare(Purchase p1, Purchase p2) {
                if (p2.getId() > p1.getId())
                    return 1;
                if (p2.getId() < p1.getId())
                    return -1;
                return 0;
            }
        });
        purchaseTable.setItems(FXCollections.observableList(purchases));
	}
}
