package ee.ut.math.tvt.salessystem.ui.controllers;

import javafx.util.converter.DoubleStringConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyDoubleStringConverter extends DoubleStringConverter {

    private static final Logger log = LogManager.getLogger(DoubleStringConverter.class);

    @Override
    public Double fromString(final String value) {
        return value.isEmpty() ? null :
                super.fromString(value);
    }

    public boolean isNumber(String value) {
        int size = value.length();
        for (int i = 0; i < size; i++) {
            if (!Character.isDigit(value.charAt(i))) {
                return false;
            }
        }
        return size > 0;
    }
}
