package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.net.URL;
import java.util.ResourceBundle;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;

public class StockController implements Initializable {

    private static final Logger log = LogManager.getLogger(StockController.class);

    private final SalesSystemDAO dao;

    @FXML
    private Button addItem;
    @FXML
    private TableView<StockItem> warehouseTableView;
    @FXML
    private TextField barcode;
    @FXML
    private TextField amount;
    @FXML
    private TextField name;
    @FXML
    private TextField price;

    @FXML
    private TableColumn<StockItem, Double> priceColumn;

    @FXML
    private TableColumn <StockItem, Integer> quantityColumn;

    public StockController(SalesSystemDAO dao) {
        this.dao = dao;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        log.info("Stock tab has been opened");
        refreshStockItems();
        setupPriceColumn();
        setupQuantityColumn();
        setTableEditable();
        // TODO refresh view after adding new items
    }

    @FXML
    public void refreshButtonClicked() {
        refreshStockItems();
    }

    @FXML
    public void removeButtonClicked() {
        dao.beginTransaction();
        dao.deleteStockItem(warehouseTableView.getSelectionModel().getSelectedItem());
        dao.commitTransaction();

        warehouseTableView.setItems(FXCollections.observableList(dao.findStockItems()));

        refreshStockItems();
    }

    public void addButtonClicked() {
        log.debug("Add button has been clicked, item is being added to stock");
        dao.beginTransaction();
        try{
            if(parseInt(amount.getText()) > 0){
                dao.saveStockItem(new StockItem(parseLong(barcode.getText()), name.getText(), "test", parseDouble(price.getText()), parseInt(amount.getText())));
                refreshStockItems();
                dao.commitTransaction();
            }
            else{
                log.error("Amount is less than 1");
                JOptionPane.showMessageDialog(null, "Amount is less than 1");
                dao.rollbackTransaction();
            }
        }
        catch(NumberFormatException e){
            log.error("Incorrect input");
            JOptionPane.showMessageDialog(null, "Amount is not integer");
            dao.rollbackTransaction();
        }
    }



    private void refreshStockItems() {
        log.debug("Stock is being refreshed");
        warehouseTableView.setItems(FXCollections.observableList(dao.findStockItems()));
        warehouseTableView.refresh();
    }

    private TableColumn < StockItem, ? > getTableColumn(
            final TableColumn < StockItem, ? > column, int offset) {
        int columnIndex = warehouseTableView.getVisibleLeafIndex(column);
        int newColumnIndex = columnIndex + offset;
        return warehouseTableView.getVisibleLeafColumn(newColumnIndex);
    }

    @SuppressWarnings("unchecked")
    private void editFocusedCell() {
        final TablePosition<StockItem, ? > focusedCell = warehouseTableView
                .focusModelProperty().get().focusedCellProperty().get();
        warehouseTableView.edit(focusedCell.getRow(), focusedCell.getTableColumn());
    }

    @SuppressWarnings("unchecked")
    private void selectPrevious() {
        if (warehouseTableView.getSelectionModel().isCellSelectionEnabled()) {
            // in cell selection mode, we have to wrap around, going from
            // right-to-left, and then wrapping to the end of the previous line
            TablePosition <StockItem, ? > pos = warehouseTableView.getFocusModel()
                    .getFocusedCell();
            if (pos.getColumn() - 1 >= 0) {
                // go to previous row
                warehouseTableView.getSelectionModel().select(pos.getRow(),
                        getTableColumn(pos.getTableColumn(), -1));
            } else if (pos.getRow() < warehouseTableView.getItems().size()) {
                // wrap to end of previous row
                warehouseTableView.getSelectionModel().select(pos.getRow() - 1,
                        warehouseTableView.getVisibleLeafColumn(
                                warehouseTableView.getVisibleLeafColumns().size() - 1));
            }
        } else {
            int focusIndex = warehouseTableView.getFocusModel().getFocusedIndex();
            if (focusIndex == -1) {
                warehouseTableView.getSelectionModel().select(warehouseTableView.getItems().size() - 1);
            } else if (focusIndex > 0) {
                warehouseTableView.getSelectionModel().select(focusIndex - 1);
            }
        }
    }


    private void setTableEditable() {
        log.debug("Table is editable");
        warehouseTableView.setEditable(true);
        // allows the individual cells to be selected
        warehouseTableView.getSelectionModel().cellSelectionEnabledProperty().set(true);
        // when character or numbers pressed it will start edit in editable
        // fields
        warehouseTableView.setOnKeyPressed(event -> {
            if (event.getCode().isLetterKey() || event.getCode().isDigitKey()) {
                editFocusedCell();
            } else if (event.getCode() == KeyCode.RIGHT ||
                    event.getCode() == KeyCode.TAB) {
                warehouseTableView.getSelectionModel().selectNext();
                event.consume();
            } else if (event.getCode() == KeyCode.LEFT) {
                // work around due to
                // TableView.getSelectionModel().selectPrevious() due to a bug
                // stopping it from working on
                // the first column in the last row of the table
                selectPrevious();
                event.consume();
            }
        });
    }

    private void setupPriceColumn() {

        priceColumn.setCellFactory(EditCell. <StockItem, Double > forTableColumn(
                new MyDoubleStringConverter()));

        priceColumn.setOnEditCommit(event -> {
            final Double value = event.getNewValue() != null ? event.getNewValue() :
                    event.getOldValue();
            ((StockItem) event.getTableView().getItems()
                    .get(event.getTablePosition().getRow()))
                    .setPrice(value);
            warehouseTableView.refresh();
        });
    }

    private void setupQuantityColumn() {

        quantityColumn.setCellFactory(EditCell. <StockItem, Integer > forTableColumn(
                new MyIntegerStringConverter()));

        quantityColumn.setOnEditCommit(event -> {
            final Integer value = event.getNewValue() != null ? event.getNewValue() :
                    event.getOldValue();
            ((StockItem) event.getTableView().getItems()
                    .get(event.getTablePosition().getRow()))
                    .setQuantity(value);
            warehouseTableView.refresh();
        });
    }
}
