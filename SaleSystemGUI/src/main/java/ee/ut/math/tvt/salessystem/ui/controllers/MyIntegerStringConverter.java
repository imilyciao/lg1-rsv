package ee.ut.math.tvt.salessystem.ui.controllers;

import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.IntegerStringConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyIntegerStringConverter extends IntegerStringConverter {

    private static final Logger log = LogManager.getLogger(IntegerStringConverter.class);

    @Override
    public Integer fromString(final String value) {
        return value.isEmpty() || !isNumber(value) ? null :
                super.fromString(value);
    }

    public boolean isNumber(String value) {
        int size = value.length();
        for (int i = 0; i < size; i++) {
            if (!Character.isDigit(value.charAt(i))) {
                return false;
            }
        }
        return size > 0;
    }
}
