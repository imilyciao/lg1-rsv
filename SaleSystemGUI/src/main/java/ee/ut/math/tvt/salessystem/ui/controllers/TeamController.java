package ee.ut.math.tvt.salessystem.ui.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the Team tab (the tab
 * labelled "Team" in the menu).
 */

public class TeamController implements Initializable {

    private static final Logger log = LogManager.getLogger(TeamController.class);

    @FXML
    private ImageView logo;

    @FXML
    private TextFlow criteria;

    @FXML
    private TextFlow info;

    public static String readFileAsString(String fileName)throws Exception
    {
        String data = "";
        data = new String(Files.readAllBytes(Paths.get(fileName)));
        return data;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        log.info("Team tab has been opened");

        Text que1=new Text("Team name \n \n");

        Text que2=new Text("Team leader \n \n");

        Text que3=new Text("Team contact \n \n");

        Text que4=new Text("Team member \n \n");

        criteria.getChildren().addAll(que1, que2, que3, que4);

        Properties prop = new Properties();
        File configFile = new File("**/application.properties");

        try {
            InputStream stream = new FileInputStream(configFile);
            prop.load(stream);
        } catch (IOException e) {
            log.error("IOException error team file not found");
            e.printStackTrace();
        }

        File file = new File(prop.getProperty("logo"));
        logo.setImage(new Image(file.toURI().toString()));

        Text ans1=new Text(prop.getProperty("teamname") + "\n \n");

        Text ans2=new Text(prop.getProperty("leader") + "\n \n");

        Text ans3=new Text(prop.getProperty("contact") + "\n \n");

        Text ans4=new Text(prop.getProperty("member") + "\n \n");

        log.debug("Team: "+prop.getProperty("teamname")+"\nLeader: "+ prop.getProperty("leader")+"\nContact: "+prop.getProperty("contact")+"\nmember: "+prop.getProperty("member"));
        info.getChildren().addAll(ans1, ans2, ans3, ans4);
    }
}